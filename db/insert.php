<?php


function insert($params) {
    $dsn = 'mysql:dbname=cv;host=cv.loc:8889';
    $user = 'root';
    $password = 'root';

    $rows = 0;

    try {
        $dbh = new \PDO($dsn, $user, $password);
        $sql = "insert into skills
               (title, procent,typeSkill)
               values 
               (:title,:procent,:typeSkill);
                   ";

        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':title', $params['title']);
        $stmt->bindParam(':procent', $params['procent']);
        $stmt->bindParam(':typeSkill', $params['typeSkill']);
        $stmt->execute();
    } catch (\PDOException $e) {
        echo 'Подключение не удалось: ' . $e->getMessage();
    }

    return $rows;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    insert($_POST);
    header('Location: http://cv.loc:7888');
}
