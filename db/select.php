<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/DBConnector.php';


if ($sql = "select id,title,procent,typeSkill from skills where typeSkill = 'professionalSkills'") {
    try {
        $this->connect = DBConnector::getInstance();
        $sth = $dbh->prepare($sql);
        $sth->execute();

        $profSkills = [];
        foreach ($sth->fetchAll(\PDO::FETCH_ASSOC) as $item) {
            $profSkills[] =[
                'id' => $item['id'],
               'title' => $item['title'],
                'procent' => $item['procent']
            ];

        }


    } catch (\PDOException $e) {
        echo 'Подключение не удалось: ' . $e->getMessage();
    }
}
if ($sql = "select  id,title,procent,typeSkill  from skills where typeSkill = 'personalSkills'"){
    try {
        $dbh = new \PDO($dsn, $user, $password);
        $sth = $dbh->prepare($sql);
        $sth->execute();

        $persSkills = [];
        foreach ($sth->fetchAll(\PDO::FETCH_ASSOC) as $item) {
            $persSkills[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'procent' => $item['procent']
            ];

        }


    } catch (\PDOException $e) {
        echo 'Подключение не удалось: ' . $e->getMessage();
    }
}

if ($sql = "select  *  from education "){
    try {
        $dbh = new \PDO($dsn, $user, $password);
        $sth = $dbh->prepare($sql);
        $sth->execute();

        $educ = [];
        foreach ($sth->fetchAll(\PDO::FETCH_ASSOC) as $item) {
            $educ[] = [
                'id' => $item['id'],
                'a' => $item['a'],
                'name' => $item['name'],
                'data' => $item['data'],
                'info' => $item['info']
            ];

        }


    } catch (\PDOException $e) {
        echo 'Подключение не удалось: ' . $e->getMessage();
    }
}

if ($sql = "select  *  from work_experience "){
    try {
        $dbh = new \PDO($dsn, $user, $password);
        $sth = $dbh->prepare($sql);
        $sth->execute();

        $workExperience = [];
        foreach ($sth->fetchAll(\PDO::FETCH_ASSOC) as $item) {
            $workExperience[] = [
                'id' => $item['id'],
                'a' => $item['a'],
                'b' => $item['b'],
                'company' => $item['company'],
                'startData' => $item['startData'],
                'endDate' => $item['endDate'],
                'diff' => $item['diff'],
                'position' => $item['position']
            ];

        }


    } catch (\PDOException $e) {
        echo 'Подключение не удалось: ' . $e->getMessage();
    }
}