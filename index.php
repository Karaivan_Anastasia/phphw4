<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include './db/select.php';
include './db/insert.php';
include './db/update.php';
include './db/delete.php';

$title = ' Material  CV/Resume';
$mailto = 'kalachovavolkova@gmail.com';
$tel = '+380988684040';
$lastName = 'Anastasia';
$firstName = 'Karaivan';
$navigator = [
    '#' => 'Home',
    '#about' => 'About',
    '#skills' => 'Skills',
    '#works' => 'Works',
    '#certificate' => 'Certificate',
    '#portfolio' => 'Portfolio',
    '#education' => 'Education',
    '#blog' => 'Blog',
    '#pricing-table' => 'Pricing',
    '#clients' => 'Client',
    '#contact' => 'Contact'
];
$myphoto = 'assets/images/proflpic.png';
$name = 'Карайван Анастасия';
$prof = 'Продавец-консультант';
$info = 'Здавствуйте,меня зовут Карайван Анастасия.На данный момент прохожу обучение на курсе Основы PHP.
Хочу преобрести знания и опыт в PHP разработке и найти работу в IT сфере .';

$btnDown = 'Download CV';
$btnCont = 'Contact Me';

$skillsTitle = 'Skills';
$proftitle = 'Professional Skills';

$professionalSkills = [
    ['title' => 'Adobe Photoshop', 'procent' => 30],
    ['title' => 'HTML', 'procent' => 15],
    ['title' => 'CSS', 'procent' => 15],
    ['title' => 'JavaScript', 'procent' => 0],
];
$perstitle = 'Personal Skills';
$personalSkills = [
    ['title' => 'Communication', 'procent' => 50],
    ['title' => 'Team Work', 'procent' => 40],
    ['title' => 'Creativity', 'procent' => 60],
    ['title' => 'Dedication', 'procent' => 70]
];
$worktitle = 'Work Experience';

$work = [
    'pozitive1' => [
        'a' => 'a2',
        'b' => '0.2s',
        'company' => 'Сеть магазинов "Pozitive"',
        'start_date' => '24.06.2020',
        'end_date' => 'now',
        'position' => 'Продавец-консультант в чайном магазине.',
        'diff' => ''
    ],
    'hirschmann' => [
        'a' => 'a3',
        'b' => '0.3s',
        'company' => 'Hirschmann Automotive VS s.r.o.',
        'start_date' => '21.01.2020',
        'end_date' => '22.03.2020',
        'diff' => '',
        'position' => 'Сотрудник отдела качества на автомобильном заводе в Чехии.'
    ],
    'pozitive2' => [
        'a' => 'a4',
        'b' => '0.4s',
        'company' => 'Сеть магазинов "Pozitive"',
        'start_date' => '11.04.2019',
        'end_date' => '14.01.2020',
        'diff' => '',
        'position' => 'Продавец-консультант в чайном магазине.'
    ],
    'dvashaga' => [
        'a' => 'a5',
        'b' => '0.5s',
        'company' => 'Cеть магазинов "Два Шага"',
        'start_date' => '10.05.2018',
        'end_date' => '4.11.2018',
        'diff' => '',
        'position' => 'Должность товароведа'],
];


$certificateTitle = 'Certificate';
$certificate = [
    'hillel1' => ['a3', '0.3s', 'Компьтерная школа "Hillel"', 'Сертификат подтверждающий что <br>Карайван Анастасия <br>закончила курс " PHP"<br>
         Директор школы Вадим Друмов
        Компьтерная школа Hillel 2021'],

    'hillel2' => ['a3', '0.3s', 'Компьтерная школа "Hillel"', 'Сертификат подтверждающий что <br>Карайван Анастасия <br>закончила курс "PHP"<br>
        Директор школы Вадим Друмов
       Компьтерная школа Hillel 2021'],

    'giometrium' => ['a3', '0.3s', 'GEOMETRIUM', 'Сертификат подтверждающий что <br>Карайван Анастасия <br>закончила базовый курс "Дизайн интерьера"<br>
         Директор школы Павел Герасимов<br>
        Дата выдачи 19.10.2019']
];

$progect = 'project.html';

$portfolioTitle = 'Portfolio';
$portfolio = [
    'all' => 'Show All',
    'category-1' => 'Art',
    'category-2' => 'Icons',
    'category-3' => 'Web Design',
    'category-4' => 'Materials',
];
$portfolioitem = [
    'minimal' => ['category-1', 'project-one', 'assets/images/p-1.jpg', 'Screenshot 01', 'Minimal Design', 'Work hard for what you want because 
                it wont come to you without a fight'],

    'lorem' => ['category-2', 'project-two', 'assets/images/p-2.jpg', 'Screenshot 01', 'Lorem Design', 'Work hard for what you want because 
                it wont come to you without a fight'],

    'creative' => ['category-1', 'project-three', 'assets/images/p-3.jpg', 'Screenshot 01', 'Creative Design', 'Lorem Ipsum because 
                it wont come to you without a fight'],

    'material' => ['category-2', 'project-four', 'assets/images/p-4.jpg', 'Screenshot 01', 'Material Design', 'Work hard for what you want because 
                    it wont come to you without a fight'],

    'clean' => ['category-4', 'project-five', 'assets/images/p-5.jpg', 'Screenshot 01', 'Clean Design', 'You want because 
                    it wont come to you without a fight'],

    'seo' => ['category-1', 'project-six', 'assets/images/p-6.jpg', 'Screenshot 01', 'SEO Optimize', 'Work hard for what you want because 
                        it wont come to you without a fight'],

    'responsive' => ['category-2', 'project-seven', 'assets/images/p-7.jpg', 'Screenshot', 'Responsive Design', 'Work hard for what you want because 
                         it wont come to you without a fight'],

    'minimal1' => ['category-3', 'project-eight', 'assets/images/p-8.jpg', 'Screenshot 01', 'Minimal Design', 'Work hard for what you want need lorem ipsuum. ']
];

$market = ['Home', 'About', 'Skills', 'Works', 'Certificate', 'Portfolio',];
$educationTitle = 'Education';
$education = [
    ['a' => '0.2s',
        'name' => 'Одесская национальная академия пищевых технологий',
        'data' => '2009-2013',
        'info' => 'Факультет компьютерной инженерии, программирования и киберзащиты. Бакалавр компьютерных наук'],
    ['a' => '0.3s',
        'name' => 'Крижановський УВК "Общеобразовательная школа I-III ступеней-лицей дошкольное учебное заведение"',
        'data' => '1998-2009',
        'info' => 'Общеобразовательная школа I-III ступеней-лицей, 11 классов']
];

$blogTitle = 'Blog';
$blog = [
    'blog1' => ['a2', '0.2s', 'blog-full-post.html', 'assets/images/b-1.jpg', 'blog-post-1', 'blog-full-post.html',
        'Website Design', 'branding, ui-ux, article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.'],

    'blog2' => ['a3', '0.3s', 'blog-full-post.html', 'assets/images/b-1.jpg', 'blog-post-2', 'blog-full-post.html',
        'Website Redesign', 'branding, ui-ux, article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.' . '<br>' . '<br>' .
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.'],

    'blog3' => ['a4', '0.4s', 'blog-full-post.html', 'assets/images/b-1.jpg', 'blog-post-3', 'blog-full-post.html',
        'Music Player Design', 'branding, ui-ux, article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.'],

    'blog4' => ['a5', '0.5s', 'blog-full-post.html', 'assets/images/b-1.jpg', 'blog-post-4', 'blog-full-post.html',
        'Marketing Partner', 'branding, ui-ux, article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.' . '<br>' . '<br>' .
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.'],

    'blog5' => ['a6', '0.6s', 'blog-full-post.html', 'assets/images/b-1.jpg', 'blog-post-5', 'blog-full-post.html',
        'Marketing Partner', 'branding, ui-ux, article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.' . '<br>' . '<br>' .
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.'],

    'blog6' => ['a7', '0.7s', 'blog-full-post.html', 'assets/images/b-1.jpg', 'blog-post-6', 'blog-full-post.html',
        'Web development ', 'branding, ui-ux, article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aliquam ornare arcu ac velit ultricies fermentum.
                    Aliquam ornare arcu ac velit ultricies fermentum.']
];
$pricingTitle = 'Pricing Table';
$pricing = [
    'startup' => ['START-UP', '$299', 'Logo', 'Domine & Hosting', 'One Page Landing', 'Email Marketing', 'Email Marketing', 'SEO', 'Order now'],
    'smallbisiness' => ['SMALL BUSINESS', '$499', 'Logo', 'Domine & Hosting', 'One Page Landing', 'PortfAdsenceolio', 'Email Marketing', 'SEO', 'Order now'],
    'enterprise' => ['ENTERPRISE', '$799', 'Logo', 'Domine & Hosting', 'One Page Landing', 'One Page Landing', 'Email Marketing', 'SEO', 'Order now'],
];

$clientTitle = 'Client';
$client = [
    'client1' => ['assets/images/team1.jpg', 'Jhon Doe', 'CEO, Creative Market', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam 
            ornare arcu ac velit ultricies fermentum. Aliquam ornare arcu ac velit ultricies fermentum..'],
    'client2' => ['assets/images/team2.jpg', 'Jhon Adnan', 'CEO, Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Aliquam ornare arcu ac velit ultricies fermentum.Aliquam ornare arcu ac velit ultricies fermentum.'],
    'client3' => ['assets/images/team3.jpg', 'Mark Anwar', 'CEO, Bdpark Market', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
            Aliquam ornare arcu ac velit ultricies fermentum. Aliquam ornare arcu ac velit ultricies fermentum.']
];

$clientLogo = [
    'a1' => 'assets/images/client1.png',
    'a2' => 'assets/images/client2.png',
    'a3' => 'assets/images/client3.png',
    'a4' => 'assets/images/client4.png',
    'a5' => 'assets/images/client6.png',
    'a6' => 'assets/images/client7.png'
];
$contactTitle = 'Contact';
$contactName = 'Full Name *';
$contactMail = 'Email *';
$contactSubgect = 'Subject *';

$burget = [
    'a1' => 'Choose your Budget',
    'a2' => '$10000-$20000',
    'a3' => '$50000-$100000',
    'a4' => '$50000-$1000000'
];
$contactMessage = 'Message *';
$butnSubmit = 'Submit';

$interest = [
    'a1' => 'https://uk-ua.facebook.com',
    'a2' => 'https://www.linkedin.com/in/анастасия-карайван-404a63205/',
    'a3' => 'mailto:kalachovavolkova@gmail.com',];


?>


<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE -->
    <title> <?= $title; ?>  </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Materialize portfolio one page template, Using for personal website."/>
    <meta name="keywords" content="cv, resume, portfolio, materialize, onepage, personal, blog"/>
    <meta name="author" content="Siful Islam, DeviserWeb">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">


    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,300,400' rel='stylesheet' type='text/css'>

    <!-- FRAMEWORK CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
    <!--<link rel="stylesheet" href="css/lightbox.css">-->

    <!-- FONT ICONS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/timeline.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css">

    <!--   COUSTOM CSS link  -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!--   COLOUR  -->
    <link rel="stylesheet" href="assets/css/color/lime.css" title="lime"> <!-- Currently Usingthis color  -->
    <!--link rel="stylesheet" href="assets/css/color/red.css" title="red"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/green.css" title="green"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/purple.css" title="purple"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/orange.css" title="orange">  -->

</head>
<body>

<!-- =========================================
                   HEADER TOP
==========================================-->
<header id="header-top"> <!--Start: "Header Area"-->
    <div class="container">
        <div class="row">
            <div class="top-contact col m12 s12 right">
                <span><i class="fa fa-envelope"></i> <a href="mailto:"> <?= $mailto; ?> </a></span>
                <span><i class="fa fa-phone"></i> <a href="tel:"><?= $tel; ?> </a></span>
            </div>
        </div>
    </div>
    <!-- =========================================
                   NAVIGATION
    ==========================================-->
    <div id="header-bottom" class="z-depth-1"> <!--Start: "Header Area"-->
        <div id="sticky-nav">
            <div class="container">
                <div class="row">
                    <nav class="nav-wrap">
                        <ul class="hide-on-med-and-down group" id="example-one">
                            <?php foreach ($navigator as $k => $menu): ?>
                                <li class="current_page_item"><a href="<?= $k ?>"><?= $menu ?></a></li>
                            <?php endforeach; ?>

                        </ul>
                        <ul class="side-nav" id="slide-out">
                            <?php foreach ($navigator as $k => $menu): ?>
                                <li class="current_page_item"><a href="<?= $k ?>"><?= $menu ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <a href="#" data-activates="slide-out" class="button-collapse"><i
                                    class="mdi-navigation-menu"></i></a>
                    </nav>
                </div>
            </div>
        </div>
    </div> <!--End: Header Area-->
</header> <!--End: Header Area-->

<!-- =========================================
                ABOUT
==========================================-->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="intro z-depth-1 col m12">
                <div class="col m12 s12">
                    <div class="profile-pic wow fadeIn a1" data-wow-delay="0.1s">
                        <img src="<?= $myphoto; ?>" alt="">
                    </div>
                </div>
                <div class="col m12 s12 co-centered wow fadeIn a2" data-wow-delay="0.2s">
                    <div class="intro-content">
                        <h2><?= $name; ?> </h2>
                        <span> <?= $prof; ?> </span>
                        <p><?= $info; ?>  </p>
                        <a href="#" class="btn waves-effect"><?= $btnDown; ?></a>
                        <a href="#contact-form" class="btn btn-success waves-effect"><?= $btnCont; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================================
                   SKILLS
==========================================-->
<section id="skills">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-gears"></i><?= $skillsTitle; ?></h2>
            </div>
            <div class="skill-line z-depth-1">
                <div class="row">
                    <div class="col m6 s12">
                        <div class="col m2 skill-icon">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                        <div class="skill-bar col m10 wow fadeIn a1" data-wow-delay="0.1s">
                            <h3><?= $proftitle; ?> </h3>
                            <?php foreach  ($profSkills as $i ) : ?>
                                <button><a href="/formUpdate.php?id=<?= $i['id'] ?>">Update</a></button>
                                <span><?= $i['title'] ?></span>
                                <a href="/db/delete.php?id=<?= $i['id'] ?>">Delete</a>
                                <div class="progress">
                                    <div class="determinate"> <?= $i['procent'] ?>%</div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col m6 s12">
                        <div class="col m2 skill-icon">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                        <div class="skill-bar col m10 wow fadeIn a2" data-wow-delay="0.2s">
                            <h3><?= $perstitle; ?></h3>
                            <?php foreach  ($persSkills as $i ) : ?>
                                <button><a href="/db/update.php?id=<?= $i['id'] ?>">Update</a></button>
                                <span><?= $i['title'] ?></span>
                                <a href="/db/delete.php?id=<?= $i['id'] ?>">Delete</a>
                                <div class="progress">
                                    <div class="determinate"> <?= $i['procent'] ?>%</div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contact-form">
    <div class="container">
        <div class="row">
            <div class="contact-form z-depth-1" id="contact">
                <div class="row">
                    <form action="./db/insert.php" method="post" id="">
                        <input type="hidden" name="id" value="">
                        <div class="input-field col m6 s12 wow fadeIn a2" data-wow-delay="0.2s">
                            <label for="name" class="h4">Skill</label>
                            <input type="text" name = 'title' class="form-control validate" id="title" required
                                   data-error="NEW ERROR MESSAGE">
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a3" data-wow-delay="0.3s">
                            <label for="name" class="h4">Procent</label>
                            <input type="text" name = 'procent' class="form-control validate" id="procent" required>
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a3" data-wow-delay="0.3s">
                            <label for="name" class="h4">Type Skill</label>
                            <input type="text" name = 'typeSkill' class="form-control validate" id="typeSkill" required>
                        </div>
                        <button type="submit" id="form-submit" class="btn btn-success waves-effect wow fadeIn a7"
                                data-wow-delay="0.7s">Submit</button>
                    </form>
                </div>
            </div>
        </div>
</section>
<!-- ========================================
        WORKS EXPERIENCE
==========================================-->
<section id="works">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h2><i class="fa fa-suitcase"> </i><?= $worktitle; ?></h2>
            </div>
            <div id="cd-timeline" class="cd-container">
                <?php foreach ($workExperience as $i) : ?>
                    <div class="cd-timeline-block wow fadeIn <?= $i['a']; ?>" data-wow-delay="<?= $i['b']; ?>">
                        <div class="cd-timeline-img">
                        </div> <!-- cd-timeline-img -->
                        <div class="cd-timeline-content col m5 s12 z-depth-1">
                            <a href=""><h2><?= $i['company']; ?> </h2></a>
                            <p><b><?= $i['position']; ?></b></p>
                            <p><b>Начальная дата: <?= $i['startData']; ?><br></b></p>
                            <p><b>Конечная дата:<?= $i['endDate']; ?><br></b></p>
                            <?php $start = new DateTime($i['startData']);
                            $end = new DateTime($i['endDate']);
                            $diff = $start->diff($end);
                            $i['diff'] = $diff->format('%m месяцев,%d дней' . '<br>'); ?>
                            <p><b><?= $i['diff']; ?><br></b></p>

                        </div> <!-- cd-timeline-content -->
                    </div><!-- cd-timeline-block -->
                <?php endforeach; ?>
            </div> <!-- cd-timeline -->
        </div>
    </div>
</section>
<section id="certificate">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h2><i class="fa fa-certificate"> </i><?= $certificateTitle; ?></h2>
            </div>
            <div id="cd-timeline" class="cd-container">
                <?php foreach ($certificate as list($a, $b, $c, $d,)) : ?>
                    <div class="cd-timeline-block wow fadeIn <?= $a; ?>" data-wow-delay="<?= $b; ?>">
                        <div class="cd-timeline-img">
                        </div> <!-- cd-timeline-img -->
                        <div class="cd-timeline-content col m5 s12 z-depth-1">
                            <a href=""><h2><?= $c; ?></h2></a>
                            <p><?= $d; ?></p>
                        </div> <!-- cd-timeline-content -->
                    </div><!-- cd-timeline-block -->
                <?php endforeach; ?>
            </div> <!-- cd-timeline -->
        </div>
    </div>
</section>

<!-- =========================================
            PORTFOLIO
==========================================-->
<section id="portfolio">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-th-list"></i><?= $portfolioTitle; ?></h2>
            </div>
            <div class="col m12 s12 portfolio-nav">
                <ul>
                    <?php foreach ($portfolio as $k => $port): ?>
                        <li class="filter" data-filter="all"><?= $port; ?></li>
                        <li class="filter" data-filter=".<?= $k; ?>"></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div id="loader">
                <div class="loader-icon"></div>
            </div>
            <div class="screenshots" id="portfolio-item">
                <div class="row">
                    <ul class="grid">
                        <!-- Portfolio one-->
                        <?php foreach ($portfolioitem as list($a, $b, $c, $d, $e, $f)) : ?>
                            <li class="col m3 s12 mix <?= $a; ?>">
                                <a href="project.html" class="sa-view-project-detail" data-action="#<?= $b; ?>">
                                    <figure class="more">
                                        <img src="<?= $c; ?>" alt="<?= $d; ?>">
                                        <figcaption>
                                            <div class="caption-content">
                                                <div class="single_image">
                                                    <h2><?= $e; ?></h2>
                                                    <p><?= $f; ?></p>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                            <!-- Portfolio two-->
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>

            <!-- PROJECT DETAILS WILL BE LOADED HERE -->
            <div class="sa-project-gallery-view" id="project-gallery-view"></div>
            <div class="back-btn col s12">
                <a id="back-button" class="btn btn-info waves-effect" href="#"><i class="fa fa-long-arrow-left"></i> Go
                    Back </a>
            </div>

            <!-- =========================================
                    MARKET PLACE
            ==========================================-->
            <div class="market-place col m12 s12 z-depth-1 wow fadeIn a3" data-wow-delay="0.3s">
                <ul>
                    <?php foreach ($market as $place): ?>
                        <li><a href=""><h3><?= $place ?></h3></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- =========================================
        EDUCATION
==========================================-->

<section id="education">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-graduation-cap"></i><?= $educationTitle; ?> </h2>
            </div>

            <div class="cd-container" id="ed-timeline">
                <?php foreach ($educ as $i): ?>
                    <div class="cd-timeline-block wow fadeIn a2" data-wow-delay="<?= $i['a']; ?>">
                        <div class="cd-timeline-img">
                        </div> <!-- cd-timeline-img -->
                        <div class="cd-timeline-content col m5 s12 z-depth-1">
                            <a href=""><h2><?= $i['name']; ?></h2></a>
                            <span> <?= $i['data']; ?></span>
                            <p><?= $i['info']; ?></p>
                        </div> <!-- cd-timeline-content -->
                    </div> <!-- cd-timeline-block -->
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- =========================================
        BLOG
==========================================-->
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-edit"> </i><?= $blogTitle; ?></h2>
            </div>
            <div class="row">
                <div class="blog">
                    <?php foreach ($blog as list($a, $b, $c, $d, $e, $f, $g, $h, $i)) : ?>
                        <div class="col m4 s12 blog-post wow fadeIn <?= $a; ?>" data-wow-delay="<?= $b; ?>">
                            <div class="thumbnail z-depth-1 animated">
                                <a href="<?= $c; ?>"><img src="<?= $d; ?>" alt="" class="responsive-img"></a>
                                <div class="blog-details">
                                    <div class="post-title" id="<?= $e; ?>">
                                        <a href="<?= $f; ?>">
                                            <h2><?= $g; ?></h2>
                                            <span><?= $h; ?></span>
                                        </a>
                                    </div>
                                    <div class="post-details">
                                        <p><?= $i; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================================
          PRICING
 ==========================================-->
<section id="pricing-table">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa  fa-money "></i><?= $pricingTitle; ?> </h2>
            </div>
            <div class="pricing wow fadeIn a3" data-wow-delay="0.3s">
                <div class="row">
                    <?php foreach ($pricing as list($a, $b, $c, $d, $e, $f, $g, $h, $i)) : ?>
                        <div class="plan col m4 s12">
                            <ul class="thumbnail z-depth-1">
                                <li><strong><?= $a; ?></strong></li>
                                <li><h3><?= $b; ?></h3></li>
                                <li>
                                    <div class="span"> <?= $c; ?></div>
                                </li>
                                <li>
                                    <div class="span"> <?= $d; ?></div>
                                </li>
                                <li>
                                    <div class="span"> <?= $e; ?></div>
                                </li>
                                <li>
                                    <div class="span"> <?= $f; ?></div>
                                </li>
                                <li>
                                    <div class="span"> <?= $g; ?></div>
                                </li>
                                <li>
                                    <div class="span"> <?= $h; ?></div>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-info waves-effect"><?= $i; ?></button>
                                </li>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- =========================================
        CLIENTS
==========================================-->
<section id="clients">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-quote-left"></i><?= $clientTitle; ?></h2>
            </div>
            <div class="clients-quates wow fadeIn a3" data-wow-delay="0.3s">
                <div class="row">
                    <?php foreach ($client as list($a, $b, $c, $d)) : ?>
                        <div class="quates col m4 s12">
                            <div class="thumbnail z-depth-1">
                                <img src="<?= $a; ?>" alt="">
                                <h3><?= $b; ?></h3>
                                <span><?= $c; ?></span>
                                <p><?= $d; ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="clear"></div>
            <!-- =========================================
                    CLIENTS LOGO
            ==========================================-->
            <div class="clients-logo z-depth-1">
                <div class="row">
                    <ul class="wow fadeIn a4" data-wow-delay="0.4s">
                        <?php foreach ($clientLogo as $logo): ?>
                            <li class="col m2 s6"><a href=""><img src="<?= $logo ?>" alt="" class="responsive-img"></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================================
        CONTACT
==========================================-->
<section id="contact-form">
    <div class="container">
        <div class="row">
            <div class="section-title wow fadeIn a1" data-wow-delay="0.1s">
                <h2><i class="fa fa-send"></i><?= $contactTitle ?></h2>
            </div>
            <div class="contact-form z-depth-1" id="contact">
                <div class="row">
                    <form id="contactForm">
                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                        <div class="input-field col m6 s12 wow fadeIn a2" data-wow-delay="0.2s">
                            <label for="name" class="h4"><?= $contactName ?></label>
                            <input type="text" class="form-control validate" id="name" required
                                   data-error="NEW ERROR MESSAGE">
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a4" data-wow-delay="0.4s">
                            <label for="email" class="h4"><?= $contactMail ?></label>
                            <input type="email" class="form-control validate" id="email" required>
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a3" data-wow-delay="0.3s">
                            <label for="last_name" class="h4"><?= $contactSubgect ?></label>
                            <input type="text" class="form-control validate" id="last_name" required>
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a5" data-wow-delay="0.5s">
                            <select>
                                <?php foreach ($burget as $k => $b): ?>
                                    <option value="<?= $k ?>"><?= $b ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-field col m12 s12 wow fadeIn a6" data-wow-delay="0.6s">
                            <label for="message" class="h4 "><?= $contactMessage ?></label>
                            <textarea id="message" class="form-control materialize-textarea validate"
                                      required></textarea>
                        </div>
                        <button type="submit" id="form-submit" class="btn btn-success waves-effect wow fadeIn a7"
                                data-wow-delay="0.7s"><?= $butnSubmit ?></button>

                    </form>
                </div>
            </div>

            <!-- =========================================
                    INTEREST
            ==========================================-->

            <div class="interests col s12 m12 l12 wow fadeIn" data-wow-delay="0.1s">
                <ul class="z-depth-1"> <!-- interetsr icon start -->
                    <li><a href="<?= $interest['a1'] ?>"><i class="fa fa-facebook-official tooltipped col m2 s6"
                                                            data-position="top" data-delay="50"
                                                            data-tooltip="Facebook"></i></a></li>
                    <li><a href="<?= $interest['a2'] ?>"><i class="fa fab fa-linkedin tooltipped col m2 s6"
                                                            data-position="top" data-delay="50"
                                                            data-tooltip="Linkedin"></i></a></li>
                    <li><a href="<?= $interest['a3'] ?>"><i class="fa fa-at tooltipped col m2 s6" data-position="top"
                                                            data-delay="50" data-tooltip="Email"></i></a></li>

                </ul> <!-- interetsr icon end -->
            </div>
        </div>
    </div>
    </div>
</section>

<!-- SCRIPTS -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.95.3/js/materialize.min.js'></script>
<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
<script src="assets/js/masonry.pkgd.js"></script>
<script src="assets/js/jquery.fancybox.pack.js"></script>
<script src="assets/js/validator.min.js"></script>
<script src="assets/js/jquery.nav.js"></script>
<script src="assets/js/modernizr.js"></script>
<script src="assets/js/jquery.sticky.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/init.js"></script>


<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE -->
    <title> <?= $title; ?>  </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Materialize portfolio one page template, Using for personal website."/>
    <meta name="keywords" content="cv, resume, portfolio, materialize, onepage, personal, blog"/>
    <meta name="author" content="Siful Islam, DeviserWeb">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">


    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,300,400' rel='stylesheet' type='text/css'>

    <!-- FRAMEWORK CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
    <!--<link rel="stylesheet" href="css/lightbox.css">-->

    <!-- FONT ICONS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/timeline.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css">

    <!--   COUSTOM CSS link  -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!--   COLOUR  -->
    <link rel="stylesheet" href="assets/css/color/lime.css" title="lime"> <!-- Currently Usingthis color  -->
    <!--link rel="stylesheet" href="assets/css/color/red.css" title="red"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/green.css" title="green"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/purple.css" title="purple"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/orange.css" title="orange">  -->

</head>
<body>
