<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//array_flip -меняет ключ и значение местами

$imput = array('blacktea', 'coffe', 'kakao', 'greentea');
$res = [];
foreach ($imput as $key => $item) {
    $res[$item] = $key;
}
var_export($res);
echo '<br>' . '<br>';

//array_wall - Применяет заданую функцию ко всем элементам указанных массивов

function sum($a)
{
    return $a + 2;
}

$arr = [1.5, 3, 27, 18];
$result = [];
foreach ($arr as $i) {
    $result[] = sum($i);
}
var_dump($result);
echo '<br>' . '<br>';

//array_map - Применяет callback-функцию ко всем элементам указанных массивов

$num = [30, 5, 66, 100];
function arrCb($num = [], callable $cb = null)
{
    if ($cb) {
        foreach ($num as $it) {
            $cbResalt[] = $cb($it);
        }
        return $cbResalt;
    } else
        return $num;
}

$resFun = arrCb($num, function ($multiply) {
    return $multiply * 2;
});
print_r($resFun);
echo '<br>' . '<br>';

//array_column — Возвращает массив из значений одного столбца входного массива

$ed = [
    [
        'name' => 'Одесская национальная академия пищевых технологий',
        'data' => '2009-2013',
        'info' => 'Факультет компьютерной инженерии, программирования и киберзащиты. Бакалавр компьютерных наук'],
    [
        'name' => 'Крижановський УВК "Общеобразовательная школа I-III ступеней-лицей дошкольное учебное заведение"',
        'data' => '1998-2009',
        'info' => 'Общеобразовательная школа I-III ступеней-лицей, 11 классов']
];
$resultEd = [];
function newarray($ed)
{
    for ($u = 0, $edCount = count($ed); $u < $edCount; $u++) {
        $resultEd[] = $ed[$u]['data'];
    }
    return $resultEd;
}

echo '<pre>';
var_export(newarray($ed));
echo '</pre>';
echo '<br>' . '<br>';

//array_chunk — Разбивает массив на части

$assor = array('arabica', 'robusta', 'coffe-crema', 'kakao', 'greentea', 'blacktea', 'milkoolong', 'tieguanyin');
$size = 4;
function break_array($assor, $size)
{
    $arrays = array();
    $i = 0;
    foreach ($assor as $index => $item) {
        if ($i++ % $size == 0) {
            $arrays[] = array();
            $current = &$arrays[count($arrays) - 1];
        }
        $current[] = $item;
    }
    return $arrays;
}

echo '<pre>';
print_r(break_array($assor, $size));
echo '</pre>';

?>


<?php

// на 100 баллов
echo 'на 100 баллов' . '<br>';

class TimeOutHelper
{
    public function setTimeout($cb, $milliSec = 0)
    {
        sleep($milliSec);
        $cb();
    }

    public function setInterval($cb, $milliSec = 0, $countIteration = 5)
    {
        for ($milliSec = 0, $countIteration = 5; $milliSec < $countIteration; ++$milliSec) {
            sleep($milliSec);
            $cb();
        }
    }
}

$cb = date("H:i:s");
(new TimeOutHelper())->setTimeout(function () {
    echo date("H:i:s") . ' - function setTimeout' . '<br>';
});
echo '<br>';
(new TimeOutHelper())->setInterval(function () {
    echo date("H:i:s") . ' - function setInterval' . '<br>';
});






