<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE -->
    <title> update </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Materialize portfolio one page template, Using for personal website."/>
    <meta name="keywords" content="cv, resume, portfolio, materialize, onepage, personal, blog"/>
    <meta name="author" content="Siful Islam, DeviserWeb">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">


    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,300,400' rel='stylesheet' type='text/css'>

    <!-- FRAMEWORK CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
    <!--<link rel="stylesheet" href="css/lightbox.css">-->

    <!-- FONT ICONS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/timeline.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css">

    <!--   COUSTOM CSS link  -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!--   COLOUR  -->
    <link rel="stylesheet" href="assets/css/color/lime.css" title="lime"> <!-- Currently Usingthis color  -->
    <!--link rel="stylesheet" href="assets/css/color/red.css" title="red"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/green.css" title="green"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/purple.css" title="purple"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/orange.css" title="orange">  -->

</head>
<body>



<section id="contact-form">
    <div class="container">
        <div class="row">
            <div class="contact-form z-depth-1" id="contact">
                <div class="row">
                    <form action="./db/update.php" method="post" id="">
                        <input type="hidden" name="id" value="<?= $_GET['id']?>">
                        <div class="input-field col m6 s12 wow fadeIn a2" data-wow-delay="0.2s">
                            <label for="name" class="h4">Skill</label>
                            <input type="text" name = 'title' class="form-control validate" id="title" required
                                   data-error="NEW ERROR MESSAGE">
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a3" data-wow-delay="0.3s">
                            <label for="name" class="h4">Procent</label>
                            <input type="text" name = 'procent' class="form-control validate" id="procent" required>
                        </div>
                        <div class="input-field col m6 s12 wow fadeIn a3" data-wow-delay="0.3s">
                            <label for="name" class="h4">Type Skill</label>
                            <input type="text" name = 'typeSkill' class="form-control validate" id="typeSkill" required>
                        </div>
                        <button type="submit" id="form-submit" class="btn btn-success waves-effect wow fadeIn a7"
                                data-wow-delay="0.7s">Submit</button>
                    </form>
                </div>
            </div>
        </div>
</section>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>
<html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- TITLE OF SITE -->
    <title> <?= $title; ?>  </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Materialize portfolio one page template, Using for personal website."/>
    <meta name="keywords" content="cv, resume, portfolio, materialize, onepage, personal, blog"/>
    <meta name="author" content="Siful Islam, DeviserWeb">

    <!-- FAVICON -->
    <link rel="icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">


    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,300,400' rel='stylesheet' type='text/css'>

    <!-- FRAMEWORK CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css">
    <!--<link rel="stylesheet" href="css/lightbox.css">-->

    <!-- FONT ICONS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


    <!-- ADDITIONAL CSS -->
    <link rel="stylesheet" href="assets/css/timeline.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css">

    <!--   COUSTOM CSS link  -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <!--   COLOUR  -->
    <link rel="stylesheet" href="assets/css/color/lime.css" title="lime"> <!-- Currently Usingthis color  -->
    <!--link rel="stylesheet" href="assets/css/color/red.css" title="red"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/green.css" title="green"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/purple.css" title="purple"> -->
    <!-- <link rel="stylesheet" href="assets/css/color/orange.css" title="orange">  -->

</head>
<body>
